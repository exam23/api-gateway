package api

import (
	//"go/token"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/exam23/api-gateway/api/docs" //swag
	v1 "gitlab.com/exam23/api-gateway/api/handlers/v1"
	"gitlab.com/exam23/api-gateway/api/middleware"
	token "gitlab.com/exam23/api-gateway/api/tokens"
	"gitlab.com/exam23/api-gateway/config"
	"gitlab.com/exam23/api-gateway/pkg/logger"
	"gitlab.com/exam23/api-gateway/services"
	"gitlab.com/exam23/api-gateway/storage/repo"
)

// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.InMemoryStorageI
	CasbinEnforcer *casbin.Enforcer
}

// New ...
// @title           exam api
// @version         1.0
// @description     This is exam server api server
// @termsOfService  2 term exam

// @contact.name   Azizbek
// @contact.url    https://t.me/azizbek_dev_2005
// @contact.email  azizbekhojimurodov@gmail.com

// @host      localhost:8080
// @BasePath  /v1

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	jwtHandler := token.JWTHandler{
		SigninKey: option.Conf.SignInKey,
		Log:       option.Logger,
	}
	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
		JWTHandler:     jwtHandler,
	})

	router.Use(middleware.NewAuth(option.CasbinEnforcer, jwtHandler, config.Load()))
	api := router.Group("/v1")
	// Customer
	api.POST("/customers", handlerV1.CreateCustomer)
	api.DELETE("/customers/:id", handlerV1.DeleteCustomer)
	api.GET("/customers/:id", handlerV1.GetCustomerInfo)
	api.PUT("/customers", handlerV1.UpdateCustomer)
	api.POST("/address", handlerV1.AddAddress)
	api.POST("/customers/register", handlerV1.RegisterCustomer)
	api.GET("/verify/:email/:code", handlerV1.Verify)
	api.GET("/login/:email/:password", handlerV1.Login)
	api.GET("/customers/list", handlerV1.GetlistCustomers)
	// Token
	api.GET("/token", handlerV1.GetAccesToken)

	// Posts
	api.POST("/posts", handlerV1.CreatePost)
	api.DELETE("/posts", handlerV1.DeletePost)
	api.PUT("/posts", handlerV1.UpdatePost)
	api.GET("/posts/byid", handlerV1.GetPostById)
	api.GET("/posts/ownerid", handlerV1.GetPostByOwnerId)
	api.GET("/posts/all", handlerV1.GetAllPosts)
	api.GET("posts/top", handlerV1.GetTop10Posts)
	api.GET("/posts/search/order", handlerV1.GetPostBySearchAndOrder)
	// Rewiews
	api.POST("/rewiews", handlerV1.CreateReweiw)
	api.PUT("/rewiews", handlerV1.UpdateRewiew)
	api.DELETE("/rewiews", handlerV1.DeleteRewiew)
	api.GET("/rewiews/postid", handlerV1.GetRewiewByPostId)

	// Admin
	api.GET("/admin/login/:admin_name/:password", handlerV1.LoginAdmin)

	// Moderator
	api.GET("/moderator/login/:moderator_name/:password", handlerV1.LoginModerator)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return router
}
