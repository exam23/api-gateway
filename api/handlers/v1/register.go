package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/spf13/cast"
	"gitlab.com/exam23/api-gateway/api/models"
	"gitlab.com/exam23/api-gateway/email"
	"gitlab.com/exam23/api-gateway/genproto/customer"
	"gitlab.com/exam23/api-gateway/pkg/etc"
	"gitlab.com/exam23/api-gateway/pkg/logger"
	"google.golang.org/protobuf/encoding/protojson"
)

// register customer
// @Summary 		register customer
// @Description 	this registers customer
// @Tags 			Customer
// @Accept 			json
// @Produce         json
// @Param           registercustomer       body  	models.CreateCustomer true "customer"
// @Success         201					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Router          /customers/register [post]
func (h *handlerV1) RegisterCustomer(c *gin.Context) {
	var body models.CreateCustomer

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, models.Error{
			Error: err,
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
		return
	}
	body.Email = strings.TrimSpace(body.Email)

	body.Email = strings.ToLower(body.Email)

	body.Password, err = etc.HashPassword(body.Password)

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,
		})
		h.log.Error("couldn't hash the password")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	emailExists, err := h.serviceManager.Customer().CheckField(ctx, &customer.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,
		})
		h.log.Error("Error while cheking email uniqeness", logger.Any("check", err))
		return
	}

	if emailExists.Exists {
		c.JSON(http.StatusConflict, models.Error{
			Error:       err,
			Description: "You have already signed up",
		})
		return
	}

	exists, err := h.redis.Exists(body.Email)
	if err != nil {
		h.log.Error("Error while checking email uniqueness")
		c.JSON(http.StatusConflict, models.Error{
			Error: err,
		})
		return
	}
	if emailExists.Exists {
		c.JSON(http.StatusConflict, models.Error{
			Error: err,
		})
		return
	}

	if cast.ToInt(exists) == 1 {
		c.JSON(http.StatusConflict, models.Error{
			Error: err,
		})
		return
	}
	customerToBeSaved := &customer.CreateCustomer{
		Id:          uuid.New().String(),
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Email:       body.Email,
		Bio:         body.Bio,
		PhoneNumber: body.PhoneNumber,
		Password:    body.Password,
	}
	for _, addres := range body.Addresses {
		customerToBeSaved.Addresses = append(customerToBeSaved.Addresses,
			&customer.Address{
				District:   addres.District,
				Street:     addres.Street,
				HomeNumber: addres.HomeNumber,
			},
		)
	}
	customerToBeSaved.Code = etc.GenerateCode(6)
	msg := "Subject: Exam email verification\n Your verification code: " + customerToBeSaved.Code
	err = email.SendEmail([]string{body.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       nil,
			Code:        http.StatusAccepted,
			Description: "Your Email is not valid, Please recheck it",
		})
		return
	}
	c.JSON(http.StatusAccepted, models.Error{
		Error:       nil,
		Code:        http.StatusAccepted,
		Description: "Your request successfuly accepted we have send code to your email, Your code is : " + customerToBeSaved.Code,
	}) // Code should be sent to gmail

	bodyByte, err := json.Marshal(customerToBeSaved)
	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
	err = h.redis.SetWithTTL(customerToBeSaved.Email, string(bodyByte), 300)
	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
}

// Verify customer
// @Summary      Verify customer
// @Description  Verifys customer
// @Tags         Customer
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        code   path string true "code"
// @Success      200  {object}  models.VerifyResponse
// @Router      /verify/{email}/{code} [get]
func (h *handlerV1) Verify(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
	)

	customerBody, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting customer from redis", logger.Any("redis", err))
	}
	customerBodys := cast.ToString(customerBody)
	body := customer.CreateCustomer{}
	err = json.Unmarshal([]byte(customerBodys), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to customer body", logger.Any("json", err))
		return
	}
	if body.Code != code {
		fmt.Println(body.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	
	// Genrating refresh and jwt tokens
	h.jwthandler.Iss = "user"
	h.jwthandler.Sub = body.Id
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	body.RefreshToken = refreshToken
	res, err := h.serviceManager.Customer().Create(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer", logger.Any("post", err))
		return
	}
	response := &models.VerifyResponse{
		Id:          res.Id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Email:       res.Email,
		Bio:         res.Bio,
		PhoneNumber: res.PhoneNumber,
	}
	for _, add := range res.Addresses {
		response.Addresses = append(response.Addresses, models.AddressResponse{
			Id:         add.Id,
			OwnerId:    add.OwnerId,
			District:   add.District,
			Street:     add.Street,
			HomeNumber: add.HomeNumber,
		})
	}

	response.JWT = accessToken
	response.RefreshToken = refreshToken

	c.JSON(http.StatusOK, response)
}
