package v1

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	t "gitlab.com/exam23/api-gateway/api/tokens"
	"gitlab.com/exam23/api-gateway/config"
	"gitlab.com/exam23/api-gateway/pkg/logger"
	"gitlab.com/exam23/api-gateway/services"
	"gitlab.com/exam23/api-gateway/storage/repo"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	cfg            config.Config
	redis          repo.InMemoryStorageI
	jwthandler     t.JWTHandler
}

// HandlerV1Config ...
type HandlerV1Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Cfg            config.Config
	Redis          repo.InMemoryStorageI
	JWTHandler     t.JWTHandler
}

// New ...
func New(c *HandlerV1Config) *handlerV1 {
	return &handlerV1{
		log:            c.Logger,
		serviceManager: c.ServiceManager,
		cfg:            c.Cfg,
		redis:          c.Redis,
		jwthandler:     c.JWTHandler,
	}
}

func GetClaims(h handlerV1, c *gin.Context) (*t.CustomClaims, error) {

	var (
		claims = t.CustomClaims{}
	)

	strToken := c.GetHeader("Authorization")

	token, err := jwt.Parse(strToken, func(t *jwt.Token) (interface{}, error) { return []byte(h.cfg.SignInKey), nil })

	if err != nil {
		h.log.Error("invalid access token")
		return nil, err
	}
	rawClaims := token.Claims.(jwt.MapClaims)

	claims.Sub = rawClaims["sub"].(string)
	claims.Exp = rawClaims["exp"].(float64)
	// fmt.Printf("%T type of value in map %v", rawClaims["exp"], rawClaims["exp"])
	// fmt.Printf("%T type of value in map %v", rawClaims["iat"], rawClaims["iat"])
	aud := cast.ToStringSlice(rawClaims["aud"])
	claims.Aud = aud
	claims.Role = rawClaims["role"].(string)
	claims.Sub = rawClaims["sub"].(string)
	claims.Token = token
	return &claims, nil
}
