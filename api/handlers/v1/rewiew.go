package v1

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/exam23/api-gateway/api/models"
	rs "gitlab.com/exam23/api-gateway/genproto/rewiew"
	"gitlab.com/exam23/api-gateway/pkg/logger"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create rewiew
// @Summary 		Create rewiew
// @Description 	this creates rewiew
// @Tags 			Rewiew
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           rewiew        body  	 rewiew.RewiewReq true "rewiew"
// @Success         201					  {object} 	rewiew.RewiewRes
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /rewiews [post]
func (h *handlerV1) CreateReweiw(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        *rs.RewiewReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Rewiew().Create(ctx, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while creting rewiew", logger.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// Delete rewiew
// @Summary 		Delete rewiew
// @Description 	this deletes rewiew
// @Tags 			Rewiew
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           id        query  string  true "Rewiew Id"
// @Success         200					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /rewiews [delete]
func (h *handlerV1) DeleteRewiew(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        rs.Id
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	body.Id = c.Query("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err := h.serviceManager.Rewiew().Delete(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while deleting post rewiew", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.Error{
		Error:       nil,
		Code:        http.StatusOK,
		Description: "post succesfully deleted",
	})
}

// Update rewiew
// @Summary 		Update rewiew
// @Description 	this Updates rewiew
// @Tags 			Rewiew
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           rewiew        body  	  models.UpdateReview true "rewiew"
// @Success         200					  {object} 	rewiew.RewiewRes
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /rewiews [put]
func (h *handlerV1) UpdateRewiew(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        models.UpdateReview
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	reviewToBeSaved := &rs.RewiewRes{
		Id:          body.Id,
		PostId:      body.PostId,
		Description: body.Description,
		Rating:      int64(body.Rating),
		CustomerId:  body.CustomerId,
	}
	response, err := h.serviceManager.Rewiew().Update(ctx, reviewToBeSaved)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while updating rewiew", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// Get rewiew
// @Summary 		Get rewiew by post Id
// @Description 	this gets rewiew by Post id
// @Tags 			Rewiew
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           id        query  string  true "Post Id"
// @Success         200					  {object} 	rewiew.GetRewiewsRes
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /rewiews/postid [get]
func (h *handlerV1) GetRewiewByPostId(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        rs.Id
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Rewiew().GetRewiewByPostId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting rewies by post id", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}
