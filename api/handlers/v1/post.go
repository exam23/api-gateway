package v1

import (
	"context"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/exam23/api-gateway/api/models"
	ps "gitlab.com/exam23/api-gateway/genproto/post"
	"gitlab.com/exam23/api-gateway/pkg/logger"
	"gitlab.com/exam23/api-gateway/pkg/utils"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create post
// @Summary 		Create post
// @Description 	this creates post
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           post        body  	 models.CreatePost true "post"
// @Success         201					  {object} 	post.Post
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Failure         401                   {object}  models.Error
// @Router          /posts [post]
func (h *handlerV1) CreatePost(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        models.CreatePost
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	postToBeSaved := &ps.CreatePost{
		OwnerId:     body.OwnerId,
		Title:       body.Title,
		Content:     body.Content,
		Description: body.Description,
	}

	for _, m := range body.Medias {
		postToBeSaved.Medias = append(postToBeSaved.Medias, &ps.Media{
			Name: m.Name,
			Link: m.Link,
			Type: m.Type,
		})
	}
	response, err := h.serviceManager.Post().Create(ctx, postToBeSaved)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while creating post", logger.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// Update post
// @Summary 		Update post
// @Description 	this Updates post
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           post        body  	  models.UpdatePost true "Update Post"
// @Success         200					  {object} 	post.Post
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts [put]
func (h *handlerV1) UpdatePost(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }

	var (
		body        models.UpdatePost
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	postToBeUpdated := &ps.Post{
		Id:          body.Id,
		OwnerId:     body.OwnerId,
		Title:       body.Title,
		Description: body.Description,
		Content:     body.Content,
	}
	for _, m := range body.Medias {
		postToBeUpdated.Medias = append(postToBeUpdated.Medias, &ps.Media{
			Id:     m.Id,
			PostId: m.PostId,
			Name:   m.Name,
			Link:   m.Link,
			Type:   m.Type,
		})
	}
	response, err := h.serviceManager.Post().Update(ctx, postToBeUpdated)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while updating post", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// Delete post
// @Summary 		Delete post
// @Description 	this deletes post
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           id        query  string  true "Post Id"
// @Success         200					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts [delete]
func (h *handlerV1) DeletePost(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        ps.Id
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err := h.serviceManager.Post().Delete(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while deleting post", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.Error{
		Error:       nil,
		Code:        http.StatusOK,
		Description: "post succesfully deleted",
	})
}

// Get post
// @Summary 		Get post
// @Description 	this gets post info by post id
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           id        query  string  true "Post Id"
// @Success         200					  {object} 	post.GetPostInfo
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts/byid [get]
func (h *handlerV1) GetPostById(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }

	var (
		body        ps.Id
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Post().GetById(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting post by id", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get posts
// @Summary 		Get posts by owner id
// @Description 	this gets posts of specific Customer
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           id        query  string  true "Owner Id"
// @Success         200					  {object} 	post.GetPosts
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts/ownerid [get]
func (h *handlerV1) GetPostByOwnerId(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        ps.Id
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	body.Id = c.Query("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Post().GetByOwnerId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting post by owner id", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get all posts
// @Summary 		Get all posts
// @Description 	this gets all the posts
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Success         200					  {object} 	post.GetPosts
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts/all [get]
func (h *handlerV1) GetAllPosts(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Post().GetAll(ctx, &ps.Empty{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting all posts", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get top 10 posts
// @Summary 		Get top 10 posts
// @Description 	this gets top 10 the posts by sorting by avg rew and then count of rewiews
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Success         200					  {object} 	post.GetPosts
// @Failure         500                   {object}  models.Error
// @Router          /posts/top [get]
func (h *handlerV1) GetTop10Posts(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.Post().GetTop10(ctx, &ps.Empty{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting top 10 posts", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, res)
}

// Get Post list by search and order by
// @Summary 		Get posts by search and order
// @Description 	Get Post list by search and order by:
// @Description     Search fields -> title, description, content
// @Description     ORDER fields -> field:DESC or field:ASC
// @Tags 			Post
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           page        query     int	    true "Page"
// @Param           limit       query     int	    true "limit"
// @Param           search      query     string    true "Search format should be 'key:value'"
// @Param           order       query     string    true "order format should be 'key:value'"
// @Success         200					  {object} 	post.GetPosts
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /posts/search/order [get]
func (h *handlerV1) GetPostBySearchAndOrder(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }

	queryParams := c.Request.URL.Query()
	search := strings.Split(c.Query("search"), ":")
	order := strings.Split(c.Query("order"), ":")
	if len(search) != 2 && len(order) != 2 {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Description: "Enter needed params",
		})
		h.log.Error("failed to get all params")
		return
	}
	params, errStr := utils.ParseQueryParams(queryParams)
	if errStr != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Description: errStr[0],
		})
		h.log.Error("failed to parse query params" + errStr[0])
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.Post().GetPostBySearchAndOrder(ctx, &ps.GetListBySearchReq{
		Page:  params.Page,
		Limit: params.Limit,
		Search: &ps.SearchFields{
			Field: search[0],
			Value: search[1],
		},
		Orders: &ps.Order{
			Field: order[0],
			Value: order[1],
		}})
	if errStr != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Description: "couldn't Get",
		})
		h.log.Error("Get .Post().GetPostBySearchAndOrder(ctx, &ps.GetListBySearchReq{", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, res)
}
