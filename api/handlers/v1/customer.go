package v1

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	_ "gitlab.com/exam23/api-gateway/api/docs" //swag
	"gitlab.com/exam23/api-gateway/api/models"
	cs "gitlab.com/exam23/api-gateway/genproto/customer"
	"gitlab.com/exam23/api-gateway/pkg/logger"
	"gitlab.com/exam23/api-gateway/pkg/utils"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create customer
// @Summary 		Create customer
// @Description 	this is expired but still staying because of exam instead use register and verify
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           createcustomer        body  	models.CreateCustomer true "customer"
// @Success         201					  {object} 	customer.Customer
// @Failure         500                   {object}  models.Error
// @Router          /customers [post]
func (h *handlerV1) CreateCustomer(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }

	var (
		body        models.CreateCustomer
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	cutomerType := &cs.CreateCustomer{
		Id:          uuid.New().String(),
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Bio:         body.Bio,
		Email:       body.Email,
		PhoneNumber: body.PhoneNumber,
		Password:    body.Password,
	}
	for _, ad := range body.Addresses {
		cutomerType.Addresses = append(cutomerType.Addresses, &cs.Address{
			District:   ad.District,
			Street:     ad.Street,
			HomeNumber: ad.HomeNumber,
		})
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Customer().Create(ctx, cutomerType)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while creating customer", logger.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// Delete customer
// @Summary 		Delete customer
// @Description 	this deletes customer by customer id
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           id        query  string  true "Customer Id"
// @Success         201					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers/{id} [delete]
func (h *handlerV1) DeleteCustomer(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", logger.Error(err))
		return
	}
	if claims.Role != "admin" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}
	var (
		body        cs.Id
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	body.Id = c.Query("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.Customer().Delete(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while deleting customer", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.Error{
		Error:       nil,
		Code:        http.StatusOK,
		Description: "Customer succesfully deleted",
	})
}

// Udate customer
// @Summary 		Udate customer
// @Description 	this udates customer
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           customer        body  	models.UpdateCustomer true "customer"
// @Success         200					  {object} 	customer.Customer
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers [put]
func (h *handlerV1) UpdateCustomer(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        models.UpdateCustomer
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	customerToBeUpdated := &cs.Customer{
		Id:          body.Id,
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Bio:         body.Bio,
		Email:       body.Email,
		PhoneNumber: body.PhoneNumber,
	}
	for _, ad := range body.Addresses {
		customerToBeUpdated.Addresses = append(customerToBeUpdated.Addresses, &cs.Address{
			Id:         ad.Id,
			OwnerId:    ad.OwnerId,
			District:   ad.District,
			Street:     ad.Street,
			HomeNumber: ad.HomeNumber,
		})
	}
	response, err := h.serviceManager.Customer().Update(ctx, customerToBeUpdated)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while updating customer", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get customer
// @Summary 		Get customer
// @Description 	this gets customer information
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           id        query  string  true "Customer Id"
// @Success         200					  {object} 	customer.GetCustomer
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers/{id} [get]
func (h *handlerV1) GetCustomerInfo(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body        cs.Id
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	body.Id = c.Query("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Customer().GetCustomerInfo(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while getting customer info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Create address to customer
// @Summary 		Create address
// @Description 	this creates address to customer
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           address        body   models.AddAddress true "address"
// @Success         200					  {object} 	customer.Address
// @Failure         500                   {object}  models.Error
// @Router          /address [post]
func (h *handlerV1) AddAddress(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }

	var (
		body        models.AddAddress
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	addressToBeSaved := &cs.Address{
		OwnerId:    body.OwnerId,
		District:   body.District,
		Street:     body.Street,
		HomeNumber: body.HomeNumber,
	}
	response, err := h.serviceManager.Customer().AddAddress(ctx, addressToBeSaved)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while adding address to customer", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get customer list by page and limit
// @Summary 		Get cutomers request
// @Description 	this gets customers list by page and limit
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           page       query      int       true "page"
// @Param           limit      query      int       true "limit"
// @Success         200					  {object} 	customer.GetListRes
// @Failure         500                   {object}  models.Error
// @Failure         401                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers/list [get]
func (h *handlerV1) GetlistCustomers(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized" {
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Code:        http.StatusUnauthorized,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }

	queryParams := c.Request.URL.Query()

	params, errStr := utils.ParseQueryParams(queryParams)
	if errStr != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Description: errStr[0],
		})
		h.log.Error("failed to parse query params" + errStr[0])
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	req := &cs.GetListReq{
		Page:  params.Page,
		Limit: params.Limit,
	}

	res, err := h.serviceManager.Customer().GetList(ctx, req)
	if errStr != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       err,
			Description: "No more content",
		})
		h.log.Error("failed to get user by page and limit" + errStr[0])
		return
	}

	c.JSON(http.StatusOK, res)
}
