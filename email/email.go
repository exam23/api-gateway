package email

import (
	"net/smtp"
	"os"
)

func SendEmail(to []string, message []byte) error {
	from := "yashildev71@gmail.com"
	password := os.Getenv("SENDEMAIL")

	// smtp server configuration.
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Sending email.
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	return err
}
