package services

import (
	"fmt"

	"gitlab.com/exam23/api-gateway/config"
	cs "gitlab.com/exam23/api-gateway/genproto/customer"

	ps "gitlab.com/exam23/api-gateway/genproto/post"
	rs "gitlab.com/exam23/api-gateway/genproto/rewiew"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type IServiceManager interface {
	Customer() cs.CustomerServiceClient
	Rewiew() rs.RewiewServiceClient
	Post() ps.PostServiceClient
}

type serviceManager struct {
	rewiewService  rs.RewiewServiceClient
	customerServie cs.CustomerServiceClient
	postService    ps.PostServiceClient
}

func (s *serviceManager) Customer() cs.CustomerServiceClient {
	return s.customerServie
}

func (s *serviceManager) Rewiew() rs.RewiewServiceClient {
	return s.rewiewService
}

func (s *serviceManager) Post() ps.PostServiceClient {
	return s.postService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {

	connRewiew, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.RewiewServiceHost, conf.RewiewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	connCustomer, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.CustomerServiceHost, conf.CustomerServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.PostServiceHost, conf.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	serviceManager := &serviceManager{
		rewiewService:  rs.NewRewiewServiceClient(connRewiew),
		customerServie: cs.NewCustomerServiceClient(connCustomer),
		postService:    ps.NewPostServiceClient(connPost),
	}

	return serviceManager, nil
}
